import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { FirebaseService, User } from '../firebase.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  emailControl: FormControl;

  constructor(private fb: FormBuilder,
              private service: FirebaseService) {
  }

  ngOnInit() {
    this.emailControl = new FormControl('', [Validators.required, Validators.email]);
    this.loginForm = this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      college: ['', Validators.required],
      year: ['', Validators.required],
      phone: ['', Validators.required],
    });
  }

  registerUser(user: User) {
    this.service.registerUser(user);
  }
}
