import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { BehaviorSubject, Observable } from 'rxjs';
import { AngularFireUploadTask } from 'angularfire2/storage';
import index from '@angular/cli/lib/cli';
import { Router } from '@angular/router';

export interface Question {
  id: string;
  index?: number;
  srcCode?: string;
  url?: string;
  question: string;
  hasImg: boolean;
  hasCode: boolean;
  options: {
    A: string;
    B: string;
    C: string;
    D: string;
  };
  answer: string;
  level: string;
}

export interface User {
  id: string;
  name: string;
  email: string;
  phone: string;
  college: string;
  year: string;
  score?: number;
}

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {
  questionCollection: AngularFirestoreCollection<Question>;
  userCollection: AngularFirestoreCollection<User>;
  questions: Observable<Question[]>;
  questionsSet = new BehaviorSubject('junior');
  curSet = this.questionsSet.asObservable();
  questionDoc: AngularFirestoreDocument<Question>;
  userDoc: AngularFirestoreDocument<User>;
  quizActive = new BehaviorSubject('inactive');
  currentStatus = this.quizActive.asObservable();
  userID = new BehaviorSubject('');
  currentID = this.userID.asObservable();
  currentScore = new BehaviorSubject(0);
  score = this.currentScore.asObservable();

  constructor(private fire: AngularFirestore,
              private router: Router) {
    this.questionCollection = this.fire.collection('easy-questions');
    this.userCollection = this.fire.collection('users');
    this.questions = this.questionCollection.valueChanges();
  }

  getQuestions() {
    return this.questions;
  }

  getQuestionByID(id: number) {
    let queryCollection: AngularFirestoreCollection<Question>;
    queryCollection = this.fire.collection('easy-questions', event => {
      return event.where('index', '==', id);
    });
    return queryCollection.valueChanges();
  }

  getEasyQuestionByID(id: number) {
    let queryCollection: AngularFirestoreCollection<Question>;
    let set: string;
    this.curSet.subscribe(value => set = value);
    if (set === 'junior') {
      queryCollection = this.fire.collection('all-questions/juniors/easy-questions', event => {
        return event.where('index', '==', id);
      });
    } else {
      queryCollection = this.fire.collection('all-questions/seniors/easy-questions', event => {
        return event.where('index', '==', id);
      });
    }
    return queryCollection.valueChanges();
  }

  getMediumQuestionByID(id: number) {
    let queryCollection: AngularFirestoreCollection<Question>;
    let set: string;
    this.curSet.subscribe(value => set = value);
    if (set === 'junior') {
      queryCollection = this.fire.collection('all-questions/juniors/medium-questions', event => {
        return event.where('index', '==', id);
      });
    } else {
      queryCollection = this.fire.collection('all-questions/seniors/hard-questions', event => {
        return event.where('index', '==', id);
      });
    }
    return queryCollection.valueChanges();
  }

  getHardQuestionByID(id: number) {
    let queryCollection: AngularFirestoreCollection<Question>;
    let set: string;
    this.curSet.subscribe(value => set = value);
    if (set === 'junior') {
      queryCollection = this.fire.collection('all-questions/juniors/hard-questions', event => {
        return event.where('index', '==', id);
      });
    } else {
      queryCollection = this.fire.collection('all-questions/seniors/hard-questions', event => {
        return event.where('index', '==', id);
      });
    }
    return queryCollection.valueChanges();
  }

  registerUser(user: User) {
    user.id = this.fire.createId();
    user.score = 0;
    this.changeUserID(user.id);
    this.changeSet(user.year);
    this.userCollection.doc(user.id).set(user).then(eve => {
      this.changeStatus('active');
      this.router.navigate(['home']);
    });
  }

  changeSet(set: string) {
    this.questionsSet.next(set);
  }

  changeStatus(stat: string) {
    this.quizActive.next(stat);
  }

  changeUserID(id: string) {
    this.userID.next(id);
  }

  changeScore(score: number) {
    this.currentScore.next(score);
  }

  updateScore(scr: number) {
    let id: string;
    this.currentID.subscribe(value => id = value);
    this.userDoc = this.userCollection.doc(id);
    this.userDoc.update({score: scr});
  }
}
