import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './home/home.component';
import {
  MatButtonModule,
  MatButtonToggleModule, MatCardModule,
  MatCheckboxModule, MatChipsModule, MatGridListModule, MatIconModule,
  MatInputModule, MatListModule, MatMenuModule, MatProgressBarModule, MatRadioModule,
  MatSelectModule, MatSidenavModule,
  MatSlideToggleModule,
  MatToolbarModule
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { environment } from '../environments/environment';
import { LayoutModule } from '@angular/cdk/layout';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { AngularFireModule } from 'angularfire2';
import { FinishComponent } from './finish/finish.component';
import { CountdownTimerModule } from 'ngx-countdown-timer';
import { LoginComponent } from './login/login.component';

import { HighlightModule } from 'ngx-highlightjs';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FinishComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule.enablePersistence(),
    AngularFireStorageModule,
    ReactiveFormsModule,
    CountdownTimerModule,
    FormsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatButtonToggleModule,
    MatChipsModule,
    MatRadioModule,
    MatProgressBarModule,
    HighlightModule.forRoot({ theme: 'dracula' })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
