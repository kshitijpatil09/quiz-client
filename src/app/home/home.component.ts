import { Component, OnDestroy, OnInit } from '@angular/core';
import { FirebaseService, Question, User } from '../firebase.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  questions: Observable<Question[]>;
  score = 0;
  skips = 3;
  easyCount: number;
  mediumCount: number;
  hardCount: number;
  selectedOption: string;
  hashEasy: boolean[];
  hashMedium: boolean[];
  hashHard: boolean[];

  timeLeft: number;
  interval: any;
  sec: number;
  min: number;
  quizActive: string;

  constructor(private service: FirebaseService,
              private router: Router) {
    let set: string;
    service.curSet.subscribe(value => set = value);
    if (set === 'junior') {
      this.easyCount = 133;
      this.mediumCount = 92;
      this.hardCount = 65;
    } else {
      this.easyCount = 142;
      this.mediumCount = 138;
      this.hardCount = 71;
    }
    this.hashEasy = [];
    this.hashMedium = [];
    this.hashHard = [];
    for (let i = 1; i <= 142; i++) {
      this.hashEasy[i] = false;
      if (i <= 138) {
        this.hashMedium[i] = false;
      }
      if (i <= 71) {
        this.hashHard[i] = false;
      }
    }
  }

  ngOnInit() {
    this.service.currentStatus.subscribe(value => this.quizActive = value);
    this.service.currentScore.subscribe(value => this.score = value);
    const id = this.getNextId('first');
    this.questions = this.service.getEasyQuestionByID(id);
    this.timeLeft = 1800;
    this.startTimer();
  }

  startTimer() {
    this.interval = setInterval(() => {
      if (this.timeLeft > 0) {
        this.timeLeft--;
        this.sec = this.timeLeft % 60;
        this.min = Math.floor(this.timeLeft / 60);
      } else {
        this.router.navigate(['finish']).then(event => {
          console.log('timeout');
          clearInterval(this.interval);
        });
      }
    }, 1000);
  }

  nextQuestion(answer: string, diff: string) {
    if (this.selectedOption === answer) {
      switch (diff) {
        case 'easy':
          this.score += 2;
          this.service.changeScore(this.score);
          this.questions = this.service.getMediumQuestionByID(this.getNextId('easy'));
          break;
        case 'medium':
          this.score += 4;
          this.service.changeScore(this.score);
          this.questions = this.service.getHardQuestionByID(this.getNextId('medium'));
          break;
        case 'hard':
          this.score += 6;
          this.service.changeScore(this.score);
          this.questions = this.service.getHardQuestionByID(this.getNextId('hard'));
          break;
      }
    } else {
      switch (diff) {
        case 'easy':
          this.score -= 1;
          this.service.changeScore(this.score);
          break;
        case 'medium':
          this.score -= 2;
          this.service.changeScore(this.score);
          break;
        case 'hard':
          this.score -= 3;
          this.service.changeScore(this.score);
          break;
      }
      this.questions = this.service.getEasyQuestionByID(this.getNextId('wrong'));
    }
    this.selectedOption = null;
  }

  skipQuestion() {
    this.skips--;
    this.questions = this.service.getEasyQuestionByID(this.getNextId('skip'));
    this.selectedOption = null;
  }

  ngOnDestroy() {
  }

  private getNextId(diff: string) {
    let id: number;
    switch (diff) {
      case 'first':
        id = Math.floor(Math.random() * this.easyCount + 1);
        this.hashEasy[id] = true;
        console.log(`easy: ${id}`);
        break;
      case 'easy':
        do {
          id = Math.floor(Math.random() * this.mediumCount + 1);
        } while (this.hashMedium[id]);
        this.hashMedium[id] = true;
        console.log(`medium: ${id}`);
        break;
      case 'medium':
      case 'hard':
        do {
          id = Math.floor(Math.random() * this.hardCount + 1);
        } while (this.hashHard[id]);
        this.hashHard[id] = true;
        console.log(`hard: ${id}`);
        break;
      case 'skip':
      case 'wrong':
        do {
          id = Math.floor(Math.random() * this.easyCount + 1);
        } while (this.hashEasy[id]);
        this.hashEasy[id] = true;
        console.log(`easy: ${id}`);
        break;
    }
    // console.log(id);
    return id;
  }
}
