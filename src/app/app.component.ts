import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'quiz-client';

  constructor(private router: Router) {
  }

  endQuiz() {
    this.router.navigate(['finish']);
  }
}
