import { Component, OnInit } from '@angular/core';
import { FirebaseService } from '../firebase.service';

@Component({
  selector: 'app-finish',
  templateUrl: './finish.component.html',
  styleUrls: ['./finish.component.scss']
})
export class FinishComponent implements OnInit {
  score: number;
  currentStatus: string;

  constructor(private service: FirebaseService) {
  }

  ngOnInit() {
    this.service.currentStatus.subscribe(value => this.currentStatus = value);
    if (this.currentStatus === 'active') {
      this.service.changeStatus('inactive');
      this.service.currentScore.subscribe(value => this.score = value);
      this.service.updateScore(this.score);
    }
  }

}
